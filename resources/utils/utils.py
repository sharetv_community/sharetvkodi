import resolveurl


class Utils:
    serie = {}

    def get_key(self, item):
        self.is_not_used()
        if item['rapidvideo']:
            return item['key'], 'rapidvideo'
        elif item['streamango']:
            return item['key'], 'streamango'

    def get_resolved(self, server, key):
        self.is_not_used()
        if server == 'rapidvideo':
           return resolveurl.resolve('https://www.rapidvideo.com/embed/' + key)
        elif server == 'streamango':
            return resolveurl.resolve('https://streamango.com/f/' + key)

    def is_not_used(self):
        pass

# -*- coding: utf-8 -*-
import json

import routing
import logging
import xbmcaddon
import requests
from resources.lib import kodiutils
from resources.lib import kodilogging
from xbmcgui import ListItem
from xbmcplugin import addDirectoryItem, endOfDirectory, setResolvedUrl
import xbmc

from resources.utils.utils import Utils

ADDON = xbmcaddon.Addon()
logger = logging.getLogger(ADDON.getAddonInfo('id'))
kodilogging.config()
plugin = routing.Plugin()

ADDON_PATH = ADDON.getAddonInfo('path')
ART = ADDON_PATH + "/resources/icons/"

utils = Utils()
SERVER = 'http://173.255.196.18:2828/'

titles_categories = [
    {
        'title': 'Todas',
        'cover': 'popular.jpg'
    },
    {
        'title': 'Terror',
        'cover': 'popular.jpg'
    },
    {
        'title': 'Acción',
        'cover': 'popular.jpg'
    },
    {
        'title': 'Infantil',
        'cover': 'popular.jpg'
    },
    {
        'title': 'Comedia',
        'cover': 'popular.jpg'
    },
    {
        'title': 'Ciencia Ficción',
        'cover': 'popular.jpg'
    },
    {
        'title': 'Aventura',
        'cover': 'popular.jpg'
    },
    {
        'title': 'Acción',
        'cover': 'popular.jpg'
    },
    {
        'title': 'Suspenso',
        'cover': 'popular.jpg'
    },
    {
        'title': 'Romance',
        'cover': 'popular.jpg'
    },
    {
        'title': 'Fantasía',
        'cover': 'popular.jpg'
    },
    {
        'title': 'Drama',
        'cover': 'popular.jpg'
    },
    {
        'title': 'Misterio',
        'cover': 'popular.jpg'
    },
    {
        'title': 'Crimen',
        'cover': 'popular.jpg'
    }
]


def get_categories():
    categories = [
        {
            'id': 'one',
            'title': 'Peliculas',
            'cover': 'popular.jpg'
        },
        {
            'id': 'two',
            'title': 'Series',
            'cover': 'popular.jpg'
        }
    ]
    for category in categories:
        addDirectoryItem(plugin.handle, plugin.url_for(
            show_genres, category['id']),
                         ListItem(category['title'], iconImage=ART + category['cover'],
                                  thumbnailImage=ART + category['cover']), True)


def get_genres_series():
    for category in titles_categories:
        addDirectoryItem(plugin.handle, plugin.url_for(
            show_series, category['title']),
                         ListItem(category['title'], iconImage=ART + category['cover'],
                                  thumbnailImage=ART + category['cover']), True)


def get_genres():
    for category in titles_categories:
        addDirectoryItem(plugin.handle, plugin.url_for(
            show_movies, category['title']),
                         ListItem(category['title'], iconImage=ART + category['cover'],
                                  thumbnailImage=ART + category['cover']), True)


def get_movies(genre):
    if genre == 'Todas':
        resp = requests.get(url=SERVER + 'movie/')
    else:
        resp = requests.get(url=SERVER + 'movie/genre/' + genre)
    movies = json.loads(resp.text)
    for movie in movies:
        infolabels = {
            'title': movie['title'],
            'year': int(movie['year']),
            'director': movie['director'],
            'genre': u','.join(movie['genre']).encode('utf-8'),
            'trailer': movie['trailer'],
            'plot': movie['synopsis'],
            'plotoutline ': movie['synopsis']
        }
        key, server = utils.get_key(movie)
        item = ListItem(movie['title'].title(), iconImage=movie['img'], thumbnailImage=movie['img'])
        item.setInfo('video', infolabels)
        addDirectoryItem(plugin.handle, plugin.url_for(
            play_movies, movie['title'].title(), server, key),
                         item, True)


def get_series(genre):
    if genre == 'Todas':
        resp = requests.get(url=SERVER + 'series/')
    else:
        resp = requests.get(url=SERVER + 'series/genre/' + genre)
    series = json.loads(resp.text)
    for serie in series:
        infolabels = {
            'title': serie['title'],
            'year': int(serie['year']),
            'director': serie['director'],
            'genre': u','.join(serie['genre']).encode('utf-8'),
            'trailer': serie['trailer'],
            'plot': serie['synopsis'],
            'plotoutline ': serie['synopsis']
        }
        item = ListItem(serie['title'].title(), iconImage=serie['img'], thumbnailImage=serie['img'])
        item.setInfo('video', infolabels)
        addDirectoryItem(plugin.handle, plugin.url_for(
            show_seasons, serie['title'].title()),
                         item, True)


def get_seasons(title):
    resp = requests.post(SERVER + 'series/search', data={'title': title})
    seasons = json.loads(resp.text)
    total = len(seasons['data'][0]['seasons'])
    for i in range(0, total):
        item = ListItem('Temporada ' + str(i + 1), iconImage=seasons['data'][0]['img'],
                        thumbnailImage=seasons['data'][0]['img'])
        addDirectoryItem(plugin.handle, plugin.url_for(
            show_chapters, seasons['data'][0]['title'], str(i)),
                         item, True)


def get_chapters(title, data):
    resp = requests.post(SERVER + 'series/search', data={'title': title})
    seasons = json.loads(resp.text)['data'][0]
    chapters = seasons['seasons'][data]['chapters']
    i = 1
    for chapter in chapters:
        key, server = utils.get_key(chapter)
        addDirectoryItem(plugin.handle, plugin.url_for(
            play_movies, 'Capitulo' + str(i), server, key),
                         ListItem('Capitulo %s' % str(i),
                                  iconImage=seasons['img'],
                                  thumbnailImage=seasons['img']), True)
        i += 1


@plugin.route('/')
def index():
    get_categories()
    endOfDirectory(plugin.handle)


@plugin.route('/genres/<category_id>')
def show_genres(category_id):
    if category_id == 'one':
        get_genres()
    else:
        get_genres_series()
    endOfDirectory(plugin.handle)


@plugin.route('/movies/<movies_id>')
def show_movies(movies_id):
    get_movies(movies_id)
    endOfDirectory(plugin.handle)


@plugin.route('/series/<series_id>')
def show_series(series_id):
    get_series(series_id)
    endOfDirectory(plugin.handle)


@plugin.route('/seasons/<title_id>')
def show_seasons(title_id):
    get_seasons(title_id)
    endOfDirectory(plugin.handle)


@plugin.route('/chapter/<title_id>/<season>')
def show_chapters(title_id, season):
    get_chapters(title_id, int(season))
    endOfDirectory(plugin.handle)


@plugin.route('/play/<title>/<server>/<key>')
def play_movies(title, server, key):
    resolved = utils.get_resolved(server=server, key=key)
    li = ListItem(title, iconImage=ART + 'popular.jpg', thumbnailImage=ART + 'popular.jpg')
    li.setProperty('IsPlayable', 'true')
    xbmc.Player().play(resolved, li)


def run():
    plugin.run()
